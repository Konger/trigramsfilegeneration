# README #

### What is this repository for? ###

This is my JAVA approach to the Kata14: Tom Swift under the Milkwood

http://codekata.com/kata/kata14-tom-swift-under-the-milkwood/


### Design Process###

**Read File**

I started out with the FileProcessor, the readFile function takes in a file name and use a Scanner to read word by word. The words are stored in a 
ArrayList called sentence for easy access. I choose ArrayList because of the O(1) access speed and it keeps the insertion order. 

When I see a ".", "!", or "?", I assume I've reached the end of the sentence. 

The entire sentence is then passed to function analyzeTrigram to break the entire sentences into 3 word trigrams. 
The first two words will be used as a key, and the third will be used for value. 


**Store trigram info into the Trigram object**

I have created a Trigrams object. It has a String key ( 1st word + 2nd word ) used for hashing, and a list to store possible values. Again ArrayList is chosen
based on easy and quick access O(1), and the ability to grow at run time. I can use a set too since all values should be unique. But with set, accessing is not as 
quick as ArrayList. In additional, I would also need to get a random value every time when a match is found, which is hard to do with set. To ensure uniqueness with
ArrayList, I can always check the collection for duplicates before I insert. 


**Store Trigram objects into a HashMap since our data is in the form of key-value pair**

I have used HashMap<String, Trigram> to store trigrams. The key for hashing is the Trigram key. The value is a Trigram object. 


**Generate New File** 

To generate a new file from the Trigram collection. I would need a starter seed. This is done by calling the function generateSeed(). The function will pick a random Trigram whose key starts with a Capital letter. After all, we can't start a new sentence with lower case letter. 

generateSeed() is a recursive function, it will only stop until it picked a key starts with a Capital letter.

To generate the entire document, I have used a recursive function called generategenerateFileWithTrigrams( Writer writer, String key, int wordCounter, int sentenceCounter, int paragraphCounter).
To break the recursive, I have used the number of sentences in a paragraph, and the number of paragraphs in the whole file as a control variable. 

To make the generated text more readable, I have used the maxWordsPerLine to add a new line break once the max number is reached. 

The sentence will end when we encounter a ".", "?", "!", or we can't find a match to continue. 

The paragraph will end when we reached maxSentencesPerParagraph, which is set by user. To start a new paragraph, 

The document generation will stop when maxParagraphs is reached.


**Punctuation marks**

Initially I have stripped off all punctuation marks and numbers. My texts were pure text. 

String cleanedWord = word.trim(); //strip away whitespaces including new lines
String strippedWord = cleanedWord.replaceAll("[^a-zA-Z]","");//strip away unwanted punctuation marks & numbers
The above approach generate text with very long sentences with the period mark only. 

I have decided to leave the punctuation marks untouched. As the result, the generated text looks more natural. I know there are issues with mismatched quotation marks, but that can be fixed with additional lines of code.