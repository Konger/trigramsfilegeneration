package test;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import junit.framework.Assert;

import org.junit.Test;

public class FileProcessorTest {

	private Scanner scanner;
	
	
	@Test
	public void testReadFile()  {
		int numSentences=0;
		int numWords=0;
		try { 
			scanner = new Scanner (new File("test.txt")); 
			ArrayList<String> sentence = new ArrayList<String>();
			while( scanner.hasNext() ) {
				String word = scanner.next();
				String cleanedWord = word.replaceAll("[^a-zA-Z]","");//strip away unwanted chars & numbers
				sentence.add(cleanedWord);
				numWords++;
				//determine when a sentence ends
				if(word.charAt(word.length()-1)=='.' || word.charAt(word.length()-1)=='!' || word.charAt(word.length()-1)=='?') {
					numSentences++;
					sentence.clear();
				}	
			}//end of while
			System.out.println("# of words read: " + numWords);
			System.out.println("# of sentences read: " + numSentences);
			
			Assert.assertEquals("failure - expected result content match", 18, numWords);
			Assert.assertEquals("failure - expected result content match", 2, numSentences);
			
		} catch(IOException e) { 
			System.err.println("Error opening file"); 
			System.exit(1); 
		} 

	}
	
	

}
