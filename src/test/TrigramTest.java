package test;

import junit.framework.Assert;

import org.junit.Test;

import com.zhiguokong.Trigram;

public class TrigramTest {
	

	@Test
	public void testConstructor() {
		String key = "I wish";
		String value = "I";
		Trigram trigram = new Trigram(key, value);
		Assert.assertEquals("failure - expected result content match", key, trigram.getKey());
		Assert.assertEquals("failure - expected result content match", value, trigram.getValue());
	}

	@Test
	public void testMultipleValues() {
		String key = "I wish";
		String value = "I";
		Trigram trigram = new Trigram(key, value);
		Assert.assertEquals("failure - expected result content match", 1, trigram.getValues().size());
		trigram.add("he");
		Assert.assertEquals("failure - expected result content match", 2, trigram.getValues().size());
	}
	
	
	@Test
	public void testDuplicateValue() {
		String key = "I wish";
		String value = "I";
		Trigram trigram = new Trigram(key, value);
		Assert.assertEquals("failure - expected result content match", 1, trigram.getValues().size());
		trigram.add(value);
		Assert.assertEquals("failure - expected result content match", 1, trigram.getValues().size());
	}
	
	@Test
	public void testNull() {
		String key = null;
		String value = "I";
		Trigram trigram = new Trigram(key, value);
		Assert.assertEquals("failure - expected result content match", 1, trigram.getValues().size());
		
	}
	
	@Test
	public void testGetSeed() {
		String key = "I wish";
		String value = "I";
		Trigram trigram = new Trigram(key, value);
		Assert.assertEquals("failure - expected result content match", "wish", trigram.getSeed());
		
	}
	
	@Test
	public void testGetRandomValue() {
		String key = "I wish";
		String value = "I";
		String value2 = "he";
		String value3 = "she";
		Trigram trigram = new Trigram(key, value);
		trigram.add(value2);
		trigram.add(value3);
		Assert.assertTrue("failure - expected result content match", trigram.getValue().matches("I|he|she"));
		
	}

}
