package com.zhiguokong;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;


/*
 * Read file and break the file into sentences
 */
public class FileProcerssor {
	
	//Hard-coded file name, change it to read different input files
	private static int maxWordsPerLine = 20; 
	
	private static int maxSentencesPerParagraph = 12;
	
	private static int maxParagraphs = 6;
	
	private HashMap<String, Trigram> trigramMap = new HashMap<String, Trigram>();
	
	
	public static void main(String[] args) {	
		
		FileProcerssor processor = new FileProcerssor();
		String inputFileName = "AliceInWonderLand.txt";
		processor.readFile(inputFileName);
		//processor.printTrigrams(); //uncomment if want to see the Trigram Mappin
		String outputFileName = "newfile.txt";
		processor.generateFile(outputFileName);
		
	}
	
	//Read text file word by word
	private void readFile( String fileName ){
		
		Scanner scanner = null; 
		try { 
			scanner = new Scanner (new File(fileName)); 
			ArrayList<String> sentence = new ArrayList<String>();
			while( scanner.hasNext() ) {
				String word = scanner.next();
				String cleanedWord = word.trim(); //strip away whitespaces including new lines
				//String strippedWord = word.replaceAll("[^a-zA-Z]","");//strip away unwanted chars & numbers
				sentence.add(cleanedWord);
				//determine when a sentence ends
				char ending = cleanedWord.charAt(cleanedWord.length()-1);
				if(ending=='.' || ending=='!' || ending=='?') {
					analyzeTrigram( sentence );
					//clear of the sentence content to be ready for next sentence
					sentence.clear();
				}
				
			}
			
			
		} catch(IOException e) { 
			System.err.println("Error opening file"); 
			System.exit(1); 
		} 

	}
	
	/*
	 * Take in a sentence and analyze the sentence into trigram
	 */
	private void analyzeTrigram( ArrayList<String> sentence ) {
		for(int i=0; i<=sentence.size()-3; i++) {
			String key = sentence.get(i) + " " + sentence.get(i+1);
			String value = sentence.get(i+2);
			
			//put trigrams into the trigram map
			if(trigramMap.containsKey(key)) { //check if already exist
				Trigram existingTrigram = trigramMap.get(key);
				existingTrigram.add(value); //add the value in, if the value already exist, it will be ignored
			}
			else {
				Trigram newTrigram = new Trigram(key, value);
				trigramMap.put(key, newTrigram);
			}
			
		}
	}
	
	/*
	 * print out the content of the Trigram Map
	 */
	public void printTrigrams() {
		Set<String> keys = trigramMap.keySet();
		for(String key : keys) {
			System.out.println(trigramMap.get(key).toString());
		}
	}
	
	/*
	 * Create a new file using trigram
	 */
	public void generateFile(String outputFileName) {
		//generate a random seed to be used as the start point
		List<String> keys = new ArrayList<String>();
		keys.addAll(trigramMap.keySet());
		String seed = generateSeed(keys);
		
		if(trigramMap.containsKey(seed)){
			Writer writer = null;
			int wordCounter = maxWordsPerLine;
			int sentenceCounter = maxSentencesPerParagraph;
			int paragraphCounter = maxParagraphs;		
			try {
				writer = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(outputFileName), "utf-8"));
				writer.append(seed);
				wordCounter--;
				generateFileWithTrigrams(writer, seed, wordCounter, sentenceCounter, paragraphCounter);
				writer.flush();
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch ( UnsupportedEncodingException e) {
				e.printStackTrace();
			}	catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/*
	 * Generate file in a recursive fashion. Stop till either find no match or reached the paragraph limit
	 */
	private void generateFileWithTrigrams( Writer writer, String key, int wordCounter, int sentenceCounter, int paragraphCounter) {
		try {
			if( trigramMap.containsKey(key) ) {
				Trigram trigram = trigramMap.get(key);
				String randomValue = trigram.getValue();//trigram will pick a random value from it's word choices
				char ending = randomValue.charAt(randomValue.length()-1);
				if(ending == '.' || ending=='!' || ending=='?') { //end of a sentence is reached. 
					sentenceCounter--;
					writer.append(" ");
					writer.append(randomValue);
					wordCounter--;
					if(sentenceCounter<=0) {
						sentenceCounter = maxSentencesPerParagraph;
						writer.append("\n\n"); //start a new paragraph
						paragraphCounter--;
					}
				} 
				else { 
					writer.append(" ");
					writer.append(randomValue);
					wordCounter--;
					if(wordCounter <= 0) {
						writer.append("\n"); //start a new line
						wordCounter = maxWordsPerLine;
					}
					
				}
				String newKey = trigram.getSeed()+" " +randomValue;
				generateFileWithTrigrams(writer, newKey, wordCounter, sentenceCounter, paragraphCounter);
					
			}
			else { //No match found, end the sentence and starts a new one from seed
				char ending = key.charAt(key.length()-1);
				if(ending != '.' && ending!='!' && ending!='?' && ending!=',') //check previous ending, make sure no .?!, existing already
					writer.append(".");
				sentenceCounter--;
				if(sentenceCounter<=0) {
					sentenceCounter = maxSentencesPerParagraph;
					writer.append("\n\n"); //start a new paragraph
					paragraphCounter--;
				}
				if(paragraphCounter>0) { //get new seed
					List<String> keys = new ArrayList<String>();
					keys.addAll(trigramMap.keySet());
					String seed = generateSeed(keys);
					generateFileWithTrigrams(writer, seed, wordCounter, sentenceCounter, paragraphCounter);
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Generate a random seed to start a paragraph. The recursive function will keep on calling itself until a phase with Capital letter is found
	 */
	private String generateSeed( List<String> keys) {
		Random randomizer = new Random();
		String randomKey = keys.get(randomizer.nextInt(keys.size()));
		if( Character.isUpperCase(randomKey.charAt(0))) {
			return randomKey;
		}
		else
			return generateSeed(keys);
		
	}
	
}
